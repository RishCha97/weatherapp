import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IONIC_DIRECTIVES } from 'ionic-angular';
import { TempraturePipe } from '../../pipes/temprature';

@Component({
  selector: 'weather',
  templateUrl: 'weather.html',
  providers: [TempraturePipe]
})
export class WeatherComponent {
  @Input() weather: Object;
  @Output() viewMore: EventEmitter<Object> = new EventEmitter();

  constructor() {
  }

  hitWeather() {
    this.viewMore.next(this.weather);
  }
}
