

import {  NavController,ModalController } from 'ionic-angular';
import { AddPage } from '../add/add';
import { Component } from '@angular/core';
import {Weather} from '../../providers/weather/weather';
//import { Observable } from 'rxjs/Observable;'
import 'rxjs/add/operator/map';
import { TempraturePipe } from '../../pipes/temprature'
import {ForcastPage } from '../forcast/forcast';
import { WeatherComponent} from '../../components/weather/weather'
import {StorageServic } from '../../providers/storage';
//import { Page } from 'ionic-angular'
@Component({
  templateUrl: 'home.html',
  providers: [Weather,TempraturePipe,WeatherComponent,StorageServic],
  
})
export class HomePage {
  public weatherList:any = [];
  public localWeather : Object;
  public Storage :StorageServic;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController , 
    public weather: Weather) {
      this.getLocalWeather();
      this.getStoredWeather();
    }

  addWeather() {
    let addWeatherModal = this.modalCtrl.create(AddPage);
    addWeatherModal.onWillDismiss( (data) =>{
      if(data){
        this.getWeather(data.city, data.country);
        
      }
      
    });
    addWeatherModal.present();
  }

  getWeather(city: string, country: string){
    this.weather.city(city,country)
    .map(data => data.json())
    .subscribe(data=>{
        this.weatherList.push(data);
        this.Storage.setWeather(data);
    },
    err=>console.log('Error'))

    //this.weatherList.push(data);
  }
  viewForcast(cityWeather){
    //console.log('hello');
    this.navCtrl.push(ForcastPage,{cityWeather:cityWeather});
  }

  getLocalWeather(){
    this.weather.local().subscribe(data=>{
      this.localWeather=data;
    })
  }
  getStoredWeather(){
          this.Storage.getWeathers().then((weathers)=>{
          this.weatherList= JSON.parse(weathers) || [];
    });
  }
 
}
