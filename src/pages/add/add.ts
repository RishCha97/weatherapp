import { Component } from '@angular/core';
import {  ViewController } from 'ionic-angular';
// import { page } from 'ionic-angular';
/*
  Generated class for the Add page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add',
  templateUrl: 'add.html'
})
export class AddPage {
  public data={
    country:"IN"
  };
  constructor(public view: ViewController) {}


  dismiss(formData){
    this.view.dismiss(formData);
  }

}
