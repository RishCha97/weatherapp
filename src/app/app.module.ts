import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddPage } from '../pages/add/add';
import {TempraturePipe} from '../pipes/temprature';
import { ForcastPage} from '../pages/forcast/forcast';
import { WeatherComponent} from '../components/weather/weather';
import { StorageServic } from '../providers/storage';
//import { SQLite} from 'ionic-native';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddPage,
    TempraturePipe,
    ForcastPage,
    WeatherComponent,
    StorageServic
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddPage,
    ForcastPage
    
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
  
  //pipe:[TempraturePipe]
})
export class AppModule {}
