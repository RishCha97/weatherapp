import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Geolocation} from 'ionic-native';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class Weather {
  private appId = '763bdb925b36dbe7153baaa5f96a7a44';
  private baseUrl = 'http://api.openweathermap.org/data/2.5/';
  constructor(public http: Http) {

  }
  city(city:string, country:string){
    let url= this.baseUrl+'weather';
    url+='?appId='+this.appId;
    url+='&q=' + city;
    url+= ','+country;
    return this.http.get(url);
  }
  forecast(cityId:string , noOfDays:number){
    let url = this.baseUrl + 'forecast/daily';
    url += '?appId='+this.appId;
    url += '&id='+ cityId;
    url += '&cnt=' + noOfDays;

    return this.http.get(url);
  }
   local(){
     let obs = Observable.create(observer =>{

     
     Geolocation.getCurrentPosition().then(resp => {
       let lat = resp.coords.latitude;
       let lng = resp.coords.longitude;

       let url = this.baseUrl + 'weather'
    url += '?appId='+this.appId;
    url+= `&lat= ${lat}&lon=${lng}`;
    
   this.http.get(url)
   .subscribe(data =>{
     observer.next(data.json());
     //return data.json();
   }),
   err=>observer.error(err),
   ()=>observer.complete();
 })
    
     })
    return obs;
  }
}
