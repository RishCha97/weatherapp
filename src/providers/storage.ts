import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from 'ionic-native'
@Injectable()
export class StorageServic {
  private db: SQLite;
  private weathers: Array<Object>;
  constructor() {
    this.db = new SQLite();
    this.db.openDatabase({
      name: this.db,
      location: 'default'
    // })//.then(() => {
    //   this.db.executeSql('create table weather(place varchar(32), temperature number)', {

    //   }).then(() => {

    //   }, (err) => {
    //     console.log('error in database :', err);
    //   })
    // }, (err) => {
    //   console.log('unable to open database :', err);
    })
  }
  getWeathers() {                                                 //Function for getting data from DB
    return this.db.readTransaction(this.weathers);              //read transactions          
  }
  setWeather(weather) {                                           //TO add in database        
    if (!this.weathers) {                                          //If list do not have the city listed inside it
      this.weathers = [weather];
    }
    else {
      this.weathers.push(weather);                              //adding to the list
    }
    this.db.databaseFeatures(this.db, JSON.stringify(this.weathers));      //updating files to database
  }
}
